<?php

namespace Aira\Promocodes\Traits;

use Aira\Promocodes\Facades\Promocodes;
use Aira\Promocodes\Model\Promocode;

trait Rewardable
{

    public function promocodes() {
        return $this->morphMany(Promocode::class, 'rewardable');
    }

    /**
     * Create promocodes for current model.
     *
     * @param int   $amount
     * @param null  $reward
     * @param array $data
     *
     * @return mixed
     */
    public function createCode($label, $amount = 1, $reward = null, array $data = [], $absolute = true, $expiration = null)
    {
        $records = [];

        // loop though each promocodes required
        foreach (Promocodes::output($amount) as $code) {
            $records[] = new Promocode([
                'label'   => $label,
                'code'   => $code,
                'reward' => $reward,
                'data'   => json_encode($data),
                'expiration' => $expiration,
                'absolute' => $absolute,
            ]);
        }

        // check for insertion of record
        if ($this->promocodes()->saveMany($records)) {
            return collect($records);
        }

        return collect([]);
    }

    /**
     * Apply promocode for rewardable and get callback.
     *
     * @param $code
     * @param $callback
     *
     * @return bool|float
     */
    public function applyCode($code, $callback = null)
    {
        $promocode = Promocode::byCode($code)->fresh()->notExpired()->first();

        // check if exists not used code
        if (!is_null($promocode)) {

            //
            if (!is_null($promocode->rewardable) && $promocode->rewardable->id !== $this->attributes['id']) {

                // callback function with false value
                if (is_callable($callback)) {
                    $callback(false);
                }

                return false;
            }

            // update promocode as it is used
            if ($promocode->update(['is_used' => true])) {

                // callback function with promocode model
                if (is_callable($callback)) {
                    $callback($promocode ?: true);
                }

                return $promocode ?: true;
            }
        }

        // callback function with false value
        if (is_callable($callback)) {
            $callback(false);
        }

        return false;
    }
}
